#!/usr/bin/env bash

vars=vars-plugin.sh
mountdir=/media/shared

cmd () {
    echo "* $@"
    "$@"
}

chd () {
    echo "[chd] $@"
    cd "$@"
}

chd /haskell-android-sdl
cmd bin/build-project apk

if [ -d "$mountdir" ]; then
    cmd source "$vars"
    echo "looky $acat_plugin_appname"
    cmd mv -vf build/bin/"$acat_plugin_appname"-debug.apk "$mountdir"
fi
