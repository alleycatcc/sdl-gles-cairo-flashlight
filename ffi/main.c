#include <stdbool.h>

#include <android/log.h>
#include <android/asset_manager.h>

#include <jni.h>

// --- taking out these includes causes app to crash.
#include "SDL.h"
#include "SDL_image.h"
#include "SDL_ttf.h"

// ------

#include <HsFFI.h>

// --- skipping __GLASGOW_HASKELL__ guards.
# include "Hslib_stub.h"

#define LOGTAG "sdlgles"

void android_info (char *str)
{
    __android_log_write (ANDROID_LOG_INFO, LOGTAG, str);
}

void android_warn (char *str)
{
    __android_log_write (ANDROID_LOG_WARN, LOGTAG, str);
}

void android_error (char *str)
{
    __android_log_write (ANDROID_LOG_ERROR, LOGTAG, str);
}

// bool register_natives ()
// {
    // JNIEnv *env = (JNIEnv*) SDL_AndroidGetJNIEnv ();
    // jclass theClass = (*env)->FindClass (env, "org/libsdl/app/SDLActivity");
//
    // jint numMethods = 1;
    // JNINativeMethod methods[] = {
    // };
//
    // jint rc = (*env)->RegisterNatives (env, theClass, methods, numMethods);
    // return 1;
// }

bool get_width_and_height (int *width, int *height)
{
    JNIEnv *env = (JNIEnv*) SDL_AndroidGetJNIEnv ();

    // @todo delete
    jclass theClass = (*env)->FindClass (env, "org/libsdl/app/SDLActivity");
    jmethodID mid = (*env)->GetStaticMethodID (env, theClass, "getDimensions", "()[I");
    jintArray resultA = (jintArray) (*env)->CallStaticObjectMethod (env, theClass, mid);

    jint *result = (*env)->GetIntArrayElements (env, resultA, NULL);
    *width = result[0];
    *height = result[1];
    char *s = (char *) malloc (100 * sizeof (char));
    sprintf (s, "height = %d, width = %d", *height, *width);
    android_info (s);

    return true;
}

bool set_torch (bool state)
{
    JNIEnv *env = (JNIEnv*) SDL_AndroidGetJNIEnv ();
    jclass theClass = (*env)->FindClass (env, "org/libsdl/app/SDLActivity");
    jmethodID mid = (*env)->GetStaticMethodID (env, theClass, "setTorch", "(Z)Z");
    jboolean ok = (jboolean) (*env)->CallStaticBooleanMethod (env, theClass, mid, state);
    return (bool) ok;
}

bool get_torch (int *state)
{
    JNIEnv *env = (JNIEnv*) SDL_AndroidGetJNIEnv ();
    jclass theClass = (*env)->FindClass (env, "org/libsdl/app/SDLActivity");
    jmethodID mid = (*env)->GetStaticMethodID (env, theClass, "getTorch", "()I");
    jint ret = (jint) (*env)->CallStaticIntMethod (env, theClass, mid, state);
    if (ret < 0) {
        android_info ("[capi]: getTorch() resulted in error");
        return false;
    }
    // android_info ("[capi] get_torch(): got:");
    // android_info (ret ? " true" : " false");
    *state = (int) ret;
    return true;
}

bool init_android ()
{
    JNIEnv *env = (JNIEnv*) SDL_AndroidGetJNIEnv ();
    jclass theClass = (*env)->FindClass (env, "org/libsdl/app/SDLActivity");
    jmethodID mid = (*env)->GetStaticMethodID (env, theClass, "init", "()Z");
    jboolean ok = (jboolean) (*env)->CallStaticBooleanMethod (env, theClass, mid);
    return (bool) ok;
}

// --- gets munged to SDL_main; other functions keep their names.
int main (int argc, char *argv[])
{
    android_info ("starting");

    if (argc != 2) {
        android_error ("Bad usage (SDL_main)");
        return 1;
    }

    android_info ("hs_init");
    hs_init (NULL, NULL);

    int width;
    int height;

    bool ok;

    ok = init_android ();
    if (!ok) {
        android_error ("Couldn't init android.");
        return 1;
    }

    // char lightState = 59;
    // ok = register_natives ();
    // if (!ok) {
        // android_error ("Couldn't register natives.");
        // return 1;
    // }

    ok = get_width_and_height (&width, &height);
    if (!ok) {
        android_error ("Couldn't get width & height.");
        return 1;
    }

    android_info ("launchit");
    // launchit (width, height, &lightState);
    launchit (width, height);

    android_info ("hs_exit");
    hs_exit ();

    return 0;
}
