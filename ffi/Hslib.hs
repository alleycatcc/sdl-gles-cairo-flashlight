module Hslib where

import           Control.Monad ( (<=<) )
import           Foreign.C ( CString, CChar, CInt (..), newCString )
import           Foreign.Ptr ( Ptr )

import           Graphics.SGC.Launch ( launch )

import           Graphics.SGC.Types ( toBool
                                    , fromBool
                                    , CApiSetTorch
                                    , CApiGetTorch
                                    , CApiCallStatus (CApiCallOk, CApiCallError, CApiCallNoop)
                                    )

foreign export ccall launchit :: CInt -> CInt -> IO ()

foreign import ccall "android_info" androidInfo   :: CString -> IO ()
foreign import ccall "android_warn" androidWarn   :: CString -> IO ()
foreign import ccall "android_error" androidError :: CString -> IO ()

foreign import ccall "set_torch" setTorch :: Bool -> IO Bool
foreign import ccall "get_torch" getTorch :: Ptr CInt -> IO Bool

launchit width height = launch (androidInfo', androidWarn', androidError') (Just (getTorch', setTorch')) dims where
    androidInfo'  = androidInfo  <=< newCString
    androidWarn'  = androidWarn  <=< newCString
    androidError' = androidError <=< newCString
    getTorch' :: CApiGetTorch
    getTorch' = fromBool <.> getTorch
    setTorch' :: CApiSetTorch
    setTorch' = fromBool <.> setTorch . toBool
    dims = Just (frint width, frint height)

frint = fromIntegral

toCApiCallStatus True  = CApiCallOk
toCApiCallStatus False = CApiCallError

-- | same fixity as <=<
infixr 1 <.>
(a <.> b) x = a <$> b x
