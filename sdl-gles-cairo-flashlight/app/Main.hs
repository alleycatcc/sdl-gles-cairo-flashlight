module Main where

import           System.Environment                 ( getArgs )
import qualified Data.ByteString.Base64     as B64  ( encode )
import           Text.Printf ( printf )
import           Data.ByteString            as BS   ( ByteString
                                                    , readFile )
import           Graphics.SGC.Launch ( launch, launchWithArgs )

type Log = String -> IO ()

main :: IO ()
main = do
    args <- getArgs
    launchWithArgs (info, warn, err) Nothing Nothing args where
    info = log' "INFO"
    warn = log' "WARN"
    err = log' "ERROR"
    log' x = putStrLn . printf "__android_log_write: %s: %s" x
