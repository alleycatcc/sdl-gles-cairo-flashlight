{-# LANGUAGE PackageImports #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE GADTs #-}

module Graphics.SGC.Types
    ( App'
    , MainLoop (MainLoop)
    , SGCAppUserData (SGCAppUserData)
    , Config
    , Color
    , Shader' (Shader'C, Shader'T)
    , ShaderVars (ShaderVarsC, ShaderVarsT)
    , Ramp (Ramp)
    , RampDirection (RampUp, RampDown)
    , RampState (RampingUp, RampingDown, RampUpper, RampLower)
    , LightState (LightOn, LightOff)
    , CApi
    , CApiCall
    , CApiSetTorch
    , CApiGetTorch
    , CApiCallStatus (CApiCallOk, CApiCallError, CApiCallNoop)
    , GraphicsTextureMappings
    , Boolable (toBool, fromBool)
    , cApiCallOk
    , rampCur
    , rampUpper
    , rampLower
    , rampStep
    , rampState
    , updateRamp
    , rampRange
    , sgcAppConfig
    , sgcAppViewportWidth
    , sgcAppViewportHeight
    , mkMatrix
    , shaderMatrix
    , shaderProgram
    , shaderShaderVars
    , mkShaderVarsC
    , mkShaderVarsT
    , configViewportWidth
    , configViewportHeight
    , mainLoopConfig
    , app'Config
    , app'ViewportWidth
    , app'ViewportHeight
    , mainLoopApp'
    , mainLoopShaders
    , mainLoopTexMaps
    , mainLoopLightState
    , mainLoopCApi
    , mainLoopModelRx
    , mainLoopModelRy
    , mainLoopModelTz
    , mainLoopT
    , mainLoopWarmth
    , mainLoopRands
    , mainLoopArgs
    , matrixVarsModel
    , matrixVarsView
    , matrixVarsProjection
    , shaderVarsCAp
    , shaderVarsCAc
    , shaderVarsCAn
    , shaderVarsTAp
    , shaderVarsTAtc
    , shaderVarsTAn
    , shaderVarsTUt
    , shaderVarsTUtim
    , shaderVarsTUdvo
    , shaderVarsTUwarmth
    , shaderVarsTUblur
    , shaderVarsTUdofog
    ) where

import           Foreign.C.Types ( CUChar, CUShort, CChar, CInt )
import           Foreign.Ptr ( Ptr )
import           Control.Applicative ( (<|>) )
import qualified Data.StateVar        as STV ( get )
import           Data.Monoid ( (<>) )

import           Data.Yaml as Y
                 ( (.:)
                 , FromJSON
                 , parseJSON )

import qualified Data.Yaml as Y
                 ( Value (Object) )

import           Graphics.Rendering.OpenGL as GL
                 ( PixelData ( PixelData )
                 , GLmatrix
                 , GLfloat
                 , GLdouble
                 , Vertex3
                 , Vertex4
                 , Program
                 , UniformLocation
                 , AttribLocation
                 , PixelFormat ( RGB, RGBA )
                 , DataType ( UnsignedByte, UnsignedShort565 )
                 , TextureObject
                 , attribLocation
                 , uniformLocation
                 )

import           Foreign
                 ( Ptr, mallocArray )

-- impl?
import           Graphics.SDLGles.GL.Util
                 ( wrapGL
                 )

import           Graphics.SDLGles.Types
                 ( App
                 , Log
                 , GraphicsTextureMapping
                 , Shader (Shader)
                 , Attrib (Attrib)
                 , Uniform (Uniform)
                 , MatrixVarsClass
                   (matrixVarsModel, matrixVarsView, matrixVarsProjection)
                 , ShaderVarsClass
                   ( shaderVarsCAp, shaderVarsCAc, shaderVarsCAn
                   , shaderVarsTAp, shaderVarsTAtc, shaderVarsTAn
                   , shaderVarsTUt )
                 , appUser
                 , attribName
                 , attribAttribLocation
                 , uniformName
                 , uniformUniformLocation
                 , shader'Program
                              , shader'ShaderVars
                              , shader'Matrix
                 )

import           Graphics.SDLGles.Util
                 ( liftA2, liftA3, liftA4, liftA5, liftA6
                 , liftA7, liftA8, liftA9, liftA10, liftA11 )

data SGCAppUserData = SGCAppUserData { sgcAppConfig :: Config
                                     , sgcAppViewportWidth :: Int
                                     , sgcAppViewportHeight :: Int }

type App' = App SGCAppUserData

app'Config = sgcAppConfig . appUser
app'ViewportWidth = sgcAppViewportWidth . appUser
app'ViewportHeight = sgcAppViewportHeight . appUser

mainLoopConfig = app'Config . mainLoopApp'

-- | these are provided as type parameters to `SGLGles.Shader`.
-- • while we don't expect MatrixVars to change, this does allow some flexibility.

data MatrixVars = MatrixVars { matrixVarsModel'      :: Uniform
                             , matrixVarsView'       :: Uniform
                             , matrixVarsProjection' :: Uniform }
                             deriving (Show, Eq)

data ShaderVars = ShaderVarsC  { shaderVarsCAp'    :: Attrib
                               , shaderVarsCAc'    :: Attrib
                               , shaderVarsCAn'    :: Attrib }
                | ShaderVarsT  { shaderVarsTAp'    :: Attrib
                               , shaderVarsTAtc'   :: Attrib
                               , shaderVarsTAn'    :: Attrib
                               , shaderVarsTUt'    :: Uniform
                               , shaderVarsTUtim   :: Uniform
                               , shaderVarsTUdvo   :: Uniform
                               , shaderVarsTUwarmth :: Uniform
                               , shaderVarsTUblur   :: Uniform
                               , shaderVarsTUdofog :: Uniform }
                  deriving (Eq, Show)

instance MatrixVarsClass MatrixVars where
    matrixVarsModel      = matrixVarsModel'
    matrixVarsView       = matrixVarsView'
    matrixVarsProjection = matrixVarsProjection'

instance ShaderVarsClass ShaderVars where
    shaderVarsCAp        = shaderVarsCAp'
    shaderVarsCAc        = shaderVarsCAc'
    shaderVarsCAn        = shaderVarsCAn'
    shaderVarsTAp        = shaderVarsTAp'
    shaderVarsTAtc       = shaderVarsTAtc'
    shaderVarsTAn        = shaderVarsTAn'
    shaderVarsTUt        = shaderVarsTUt'

mkAttribute log prog str = liftA2 Attrib name loc where
    name = pure str
    loc = (wrapGL log ("attribLocation "  <> str) . STV.get $ attribLocation  prog str)

mkUniform log prog str   = liftA2 Uniform name loc where
    name = pure str
    loc = (wrapGL log ("uniformLocation "  <> str) . STV.get $ uniformLocation  prog str)

mkMatrix log prog =
    liftA3 MatrixVars a b c where
        a = unif' "model"
        b = unif' "view"
        c = unif' "projection"
        unif' = mkUniform log prog

mkShaderVarsC log prog =
    liftA3 ShaderVarsC a b c where
        a = att' "a_position"
        b = att' "a_color"
        c = att' "a_normal"
        att' = mkAttribute log prog

mkShaderVarsT log prog =
    liftA9 ShaderVarsT a b c d e f g h i where
        a = att' "a_position"
        b = att' "a_texcoord"
        c = att' "a_normal"
        d = unif' "texture"
        e = unif' "transpose_inverse_model"
        f = unif' "do_vary_opacity"
        g = unif' "warmth"
        h = unif' "blur_amount"
        i = unif' "do_fog"
        att' = mkAttribute log prog
        unif' = mkUniform log prog

data Shader' = Shader'C (Shader MatrixVars ShaderVars)
             | Shader'T (Shader MatrixVars ShaderVars)
              deriving (Eq, Show)

shader' (Shader'C x) = x
shader' (Shader'T x) = x

shaderProgram    = shader'Program    . shader'
shaderShaderVars = shader'ShaderVars . shader'
shaderMatrix     = shader'Matrix     . shader'

type Color = (Float, Float, Float, Float)

-- | these contain the image data (in JuicyPixels form, not to be confused
--   with the backing array) and optionally the cairo frames and movie
--   timings.
-- • they get coupled to a Tex (our structure which holds the texture
--   arrays) and a TextureObject (GL's "texture name" integer).

-- | we use VertexX (not VectorX), also for the normal, because Vector just
--   makes it more complicated with no benefit.

data Config = Config { configViewportWidth :: Int
                     , configViewportHeight :: Int }

instance FromJSON Config where
    parseJSON (Y.Object v) = Config
        <$> v .: "viewportWidth"
        <*> v .: "viewportHeight"
    parseJSON _ = error "invalid type for parseJSON Config"

class Boolable a where
    toBool   :: a -> Bool
    fromBool :: Bool -> a

data LightState = LightOn | LightOff
                  deriving Show

instance Boolable LightState where
    toBool LightOn  = True
    toBool LightOff = False
    fromBool False  = LightOff
    fromBool True   = LightOn

-- | Boolable instance for CApiCall means the call was carried out (so no
--   Noop) and we want to separate Error from Ok.
-- ٭ This is how the C API interprets `bool` calls.

instance Boolable CApiCallStatus where
    toBool CApiCallError = False
    toBool _             = True
    fromBool False       = CApiCallError
    fromBool True        = CApiCallOk

data CApiCallStatus = CApiCallOk | CApiCallError | CApiCallNoop

cApiCallOk CApiCallOk = True
cApiCallOk _          = False

type CApiCall = LightState -> IO CApiCallStatus
type CApiSetTorch = LightState -> IO CApiCallStatus
type CApiGetTorch = Ptr CInt -> IO CApiCallStatus
type CApi = (CApiGetTorch, CApiSetTorch)
type GraphicsTextureMappings = (GraphicsTextureMapping, GraphicsTextureMapping, GraphicsTextureMapping)

data MainLoop = MainLoop { mainLoopApp' :: App'
                         , mainLoopShaders :: (Shader', Shader')
                         , mainLoopTexMaps :: GraphicsTextureMappings
                         , mainLoopCApi :: Maybe (CApiGetTorch, CApiSetTorch)
                         , mainLoopLightState :: LightState
                         , mainLoopModelRx :: Float
                         , mainLoopModelRy :: Float
                         , mainLoopModelTz :: Float
                         , mainLoopWarmth :: Warmth
                         , mainLoopT :: Int
                         , mainLoopRands :: [Double]
                         , mainLoopArgs :: [String] }

type Warmth = Ramp

data RampDirection = RampUp | RampDown

data RampState = RampingUp Int
               | RampingDown Int
               | RampUpper
               | RampLower
                 deriving Show

data Ramp = Ramp { rampLower :: Int
                 , rampUpper :: Int
                 , rampStep :: Int
                 , rampState :: RampState }
                 deriving Show

rampCur ramp = rampCur' . rampState $ ramp where
    rampCur' (RampingUp a)   = a
    rampCur' (RampingDown a) = a
    rampCur' RampUpper       = rampUpper ramp
    rampCur' RampLower       = rampLower ramp

updateRamp app direction ramp = update' direction . rampState $ ramp where
    update' RampUp RampUpper         = ramp
    update' RampDown RampLower       = ramp
    update' RampUp (RampingUp _)     = if l > upper then ramp { rampState = RampUpper }
                                                    else ramp { rampState = RampingUp l }
    update' RampDown (RampingDown _) = if m < lower then ramp { rampState = RampLower }
                                                    else ramp { rampState = RampingDown m }
    update' RampUp _                 = ramp { rampState = RampingUp cur }
    update' RampDown _               = ramp { rampState = RampingDown cur }
    cur = rampCur ramp
    upper = rampUpper ramp
    lower = rampLower ramp
    step = rampStep ramp
    l = cur + step
    m = cur - step

rampRange = spreadCombine rampUpper rampLower (-)

spreadCombine f g h x = h (f x) (g x)
