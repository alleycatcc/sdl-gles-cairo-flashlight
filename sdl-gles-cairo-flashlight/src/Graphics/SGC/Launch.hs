{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PackageImports #-}

module Graphics.SGC.Launch ( launch, launchWithArgs ) where

import           Prelude hiding ( log, init )

import           Foreign
                 ( ForeignPtr, peek, castPtr
                 , mallocForeignPtr, withForeignPtr, poke )
import           Foreign.Ptr ( Ptr )
import           Foreign.C ( CChar, CInt )
import qualified Data.ByteString.Base64 as B64 ( decode )
import qualified Data.ByteString as BS ( readFile )
import           Data.ByteString as BS ( ByteString )
import           Data.Function ( (&) )
import           Data.Monoid ( (<>) )
import           Data.Maybe ( isJust, fromJust )
import           Data.Either ( isLeft )
import           Data.Foldable ( foldlM )
import           Data.Bifunctor ( first )
import           Data.Bitraversable ( bitraverse )
import           GHC.Float ( int2Float )
import           Control.Applicative ( empty )
import           Text.Printf ( printf )
import           Control.Monad ( (<=<), when, unless, forM_, forM, join, void )
import           Data.Map as Dmap ( Map, toList )
import           Control.DeepSeq ( deepseq )
import qualified Data.Yaml as Y ( decodeEither' )

import           Codec.Picture      as JP ( DynamicImage, decodePng )

import           Graphics.Rendering.OpenGL as GL
                 ( Color4 (Color4)
                 , ClearBuffer ( ColorBuffer, DepthBuffer ) )

import qualified Graphics.Rendering.OpenGL as GL
                 ( clear )

import           Graphics.SDLGles.App
                 ( init, runLoop )

import           Graphics.SDLGles.Config
                 ( defaultConfig )

import           Graphics.SDLGles.GL.Util
                 ( wrapGL )

import           Graphics.SDLGles.GLES.Draw
                 ( pushColors
                 , rectangle
                 , rectangleTex
                 , triangle
                 , sphere
                 , cylinder
                 , cylinderTex
                 , circle
                 , coneSection
                 , coneSectionTex
                 , pushPositions
                 , pushPositionsWithArray
                 , pushTexCoords
                 , pushTexCoordsWithArray
                 , pushNormals
                 , pushNormalsWithArray
                 , pushAttributesVertex4
                 , pushAttributesFloat
                 , pushAttributesWithArrayVertex4
                 , pushAttributesWithArrayScalar
                 , rectangleStroke
                 )

import           Graphics.SDLGles.GLES.Coords
                 ( vec3
                 , verl3
                 , vec4
                 , vec3gld
                 , invertMajor'
                 , ver3
                 , ver3gld
                 , frustumF
                 , orthoF
                 , lookAtF
                 , normalize
                 , unProjectItF
                 , rotateX
                 , rotateY
                 , rotateZ
                 , scaleX
                 , scaleY
                 , scaleZ
                 , verple3
                 , invertMatrix
                 , identityMatrix
                 , multMatrices
                 , vecl4
                 , toMGC
                 , toMGCD
                 , translateX
                 , translateY
                 , translateZ
                 , ver3
                 , ver4
                 , vec3 )

import           Graphics.SDLGles.GLES.Shader
                 ( uniform
                 , useShader
                 , attrib )

import           Graphics.SDLGles.Texture
                 ( createTextures2DSimple
                 , activateTexture
                 , updateTexture
                 , updateTextures
                 , updateTextureCairo
                 , textureWithCairo
                 , textureNoCairo )

import           Graphics.SDLGles.Util
                 ( appReplaceModel
                 , appUpdateMatrix
                 , appMultiplyModel
                 , appMultiplyRightModel
                 , appMultiplyView
                 , replaceModel
                 , replaceView
                 , replaceProj
                 , pushModel
                 , pushView
                 , pushProj
                 , stackPop'
                 , stackReplace' )

import           Graphics.SDLGles.Util2
                 ( frint )

import           Graphics.SDLGles.Util3
                 ( map3
                 , deg2rad
                 , vcross
                 , vmag
                 , v3x
                 , v3y
                 , v3z
                 , vdot
                 , vdiv
                 , float
                 , toDeg
                 , fst3
                 , snd3
                 , thd3 )

import           Graphics.SDLGles.SDL.Events ( processEvents )

import           Graphics.SDLGles.Types
                 ( App (App)
                 , DMat
                 , Log (Log, info, warn, err)
                 , Logger
                 , appConfig
                 , appLog
                 , appMatrix
                 , appUser
                 , output565
                 , toShaderDC
                 , toShaderDT )

import qualified Graphics.SDLGles.Types as SDG
                 ( Config )

import           Graphics.SDLGles.Types
                 ( GraphicsData (GraphicsSingle, GraphicsSingleCairo, GraphicsMoving, GraphicsMovingFinite)
                 , GraphicsTextureMapping (GraphicsTextureMapping)
                 , ShaderD (ShaderDC, ShaderDT)
                 , Tex (NoTexture)
                 , attribAttribLocation
                 , getCSurf
                 , graphicsTextureMappingGraphicsData
                 , graphicsTextureMappingTexture
                 , graphicsTextureMappingTextureObject
                 , mvpConfigCubeScale
                 , mvpConfigProjectionType
                 , mvpConfigTranslateZ
                 , shaderDCAttributeColors
                 , shaderDCAttributePosition
                 , shaderDCProgram
                 , shaderDCUniformModel
                 , shaderDCUniformProjection
                 , shaderDCUniformView
                 , shaderDTAttributePosition
                 , shaderDTAttributeTexCoord
                 , shaderDTProgram
                 , shaderDTUniformModel
                 , shaderDTUniformProjection
                 , shaderDTUniformTexture
                 , shaderDTUniformView
                 , texHeight
                 , texWidth
                 , uniformUniformLocation )

import           Graphics.SGC.Config
                 ( blurOnWarm
                 , doDebug
                 , doFog
                 , doMouseZoom
                 , doMouseRotateModel
                 , dragXFactor
                 , isEmbedded
                 , jitter
                 , mouseWheelFactor
                 , mvpConfig
                 , rectangleDimension
                 , warmthCoefficient
                 , warmthRange )

import           Graphics.SGC.ImageData ( imgs )

import           Graphics.SGC.Shader
                 ( initShaderTexture
                 , initShaderColor )

import           Graphics.SGC.Types
                 ( App'
                 , Boolable ( toBool, fromBool )
                 , CApi
                 , CApiCall
                 , CApiCallStatus (CApiCallOk, CApiCallError, CApiCallNoop)
                 , CApiGetTorch
                 , CApiSetTorch
                 , Config
                 , GraphicsTextureMappings
                 , LightState (LightOn, LightOff)
                 , MainLoop (MainLoop)
                 , RampDirection (RampUp, RampDown)
                 , Ramp (Ramp)
                 , RampState (RampingUp, RampingDown, RampUpper, RampLower)
                 , SGCAppUserData (SGCAppUserData)
                 , Shader' (Shader'C, Shader'T)
                 , app'Config
                 , app'Config
                 , app'ViewportHeight
                 , app'ViewportWidth
                 , cApiCallOk
                 , configViewportHeight
                 , configViewportWidth
                 , mainLoopApp'
                 , mainLoopArgs
                 , mainLoopCApi
                 , mainLoopConfig
                 , mainLoopLightState
                 , mainLoopModelRx
                 , mainLoopModelRy
                 , mainLoopModelTz
                 , mainLoopRands
                 , mainLoopShaders
                 , mainLoopT
                 , mainLoopTexMaps
                 , mainLoopWarmth
                 , matrixVarsModel
                 , matrixVarsProjection
                 , matrixVarsView
                 , rampCur
                 , rampLower
                 , rampRange
                 , rampState
                 , rampStep
                 , rampUpper
                 , sgcAppConfig
                 , sgcAppViewportHeight
                 , sgcAppViewportWidth
                 , shaderMatrix
                 , shaderProgram
                 , shaderShaderVars
                 , shaderVarsCAc
                 , shaderVarsCAn
                 , shaderVarsCAp
                 , shaderVarsTAn
                 , shaderVarsTAp
                 , shaderVarsTAtc
                 , shaderVarsTUblur
                 , shaderVarsTUdofog
                 , shaderVarsTUdvo
                 , shaderVarsTUt
                 , shaderVarsTUtim
                 , shaderVarsTUwarmth
                 , updateRamp )

import           Graphics.SGC.Util
                 ( (<=<:)
                 , (.:.)
                 , between
                 , col8
                 , color
                 , color3
                 , color4
                 , debug
                 , die
                 , dropBack
                 , dropFront
                 , glFalseF
                 , glTrueF
                 , hsvCycle
                 , inv
                 , nAtATime
                 , randoms
                 , toRight
                 , within )

headFail :: MonadFail m => String -> [a] -> m a
headFail msg xs = case xs of
  [] -> fail msg
  x : _ -> pure x

headFailM :: MonadFail m => String -> m [a] -> m a
headFailM msg m = m >>= headFail msg

tailFail :: MonadFail m => String -> [a] -> m [a]
tailFail msg xs = case xs of
  [] -> fail msg
  _ : xs -> pure xs

tailFailM :: MonadFail m => String -> m [a] -> m [a]
tailFailM msg m = m >>= tailFail msg

launch :: (Logger, Logger, Logger) -> Maybe CApi -> Maybe (Int, Int) -> IO ()
launch x y z = launchWithArgs x y z []

launchWithArgs :: (Logger, Logger, Logger) -> Maybe CApi -> Maybe (Int, Int) -> [String] -> IO ()
launchWithArgs loggers@(info', warn', error') cApiMb dimsMb args = do
    let log = Log info' warn' error'
        debug' = debug log
        die' str = die log . (str <>) . show

    configYaml <- if isEmbedded then pure configYamlInline
                                else BS.readFile "config.yaml"

    config <- do  let  error' err = die' "Couldn't decode config.yaml: " err
                  either error' pure $ Y.decodeEither' configYaml

    let dims' Nothing = do  info' $ "Getting width & height from config"
                            pure (configViewportWidth config, configViewportHeight config)
        dims' (Just (width', height')) = do
                            info' $ "Got width and height from caller"
                            pure (width', height')

    (viewportWidth, viewportHeight) <- dims' dimsMb

    let appUserData' = SGCAppUserData config viewportWidth viewportHeight
    info' $ printf "Viewport dims: %s x %s" (show viewportWidth) (show viewportHeight)

    app <- init loggers sdlGlesConfig appUserData' mvpConfig args

    rands <- randoms

    debug' "initShaders"
    (colorShader, texShader) <- initShaders log

    (texMappingLight, texMappingExtin) <- doImgs app sdlGlesConfig

    debug' "starting loop"

    let warmth' = Ramp 0 warmthRange 1 RampLower

    runLoop app appLoop $ MainLoop
        { mainLoopApp' = app
        , mainLoopShaders = (colorShader, texShader)
        , mainLoopTexMaps = (texMappingLight, texMappingExtin, texMappingExtin)
        , mainLoopCApi = cApiMb
        , mainLoopLightState = LightOff
        , mainLoopModelRx = 0
        , mainLoopModelRy = 0
        , mainLoopModelTz = 0
        , mainLoopWarmth = warmth'
        , mainLoopT = 0 -- t
        , mainLoopRands = rands
        , mainLoopArgs = args }

extinguishSeq, lightingSeq :: [JP.DynamicImage] -> [JP.DynamicImage]
extinguishSeq imgs = imgs !! 10 : take 10 imgs
lightingSeq imgs = dropFront 2 imgs ++ imgs !! 9 : [imgs !! 10]

doImgs :: App' -> SDG.Config -> IO (GraphicsTextureMapping, GraphicsTextureMapping)
doImgs app sdlGlesConfig = do
    let log = appLog app
        delay' | isEmbedded = 1
               | otherwise = 1
        decode' = decodeImage log
    imgs' <- mapM decode' imgs

    let graphicsLight = GraphicsMovingFinite (lightingSeq imgs') 11 delay' 0
        graphicsExtin = GraphicsMovingFinite (extinguishSeq imgs') 11 delay' 0
    tex1 <- textureNoCairo sdlGlesConfig 512 512
    tex2 <- textureNoCairo sdlGlesConfig 512 512
    [texName1, texName2] <- createTextures2DSimple app 2

    let light = GraphicsTextureMapping graphicsLight tex1 texName1
        extin = GraphicsTextureMapping graphicsExtin tex2 texName2
    pure (light, extin)

appLoop :: MainLoop -> IO (Maybe MainLoop)
appLoop curLoop = do
    let log = appLog app
        info' = info log
        debug' = debug log
        debug'' str x = debug' . printf str $ show x

        texMaps    = curLoop & mainLoopTexMaps
        cApiMb     = curLoop & mainLoopCApi
        app        = curLoop & mainLoopApp'
        shaders    = curLoop & mainLoopShaders
        modelRx    = curLoop & mainLoopModelRx
        modelRy    = curLoop & mainLoopModelRy
        modelTz    = curLoop & mainLoopModelTz
        warmth     = curLoop & mainLoopWarmth
        t          = curLoop & mainLoopT
        rands      = curLoop & mainLoopRands
        args       = curLoop & mainLoopArgs

        config         = app & app'Config
        viewportWidth  = app & app'ViewportWidth
        viewportHeight = app & app'ViewportHeight

    debug' "* looping"

    wrapGL log "clear" $ GL.clear [ColorBuffer, DepthBuffer]

    rand <- headFail "no more rands" rands
    let (texMappingLight, texMappingExtin, texMappingCur) = texMaps
    texMappingCur' <- headFailM "no textures" $ updateTextures app [texMappingCur]

    ( qPressed, click, clickRelease, clickPress, dragAmounts, wheelOrPinchAmount ) <- processEvents log ( viewportWidth, viewportHeight )

    -- 'multiply right': for pushing the model away from yourself in grand,
    -- fixed system
    -- 'multiply': for relativising yourself and drawing locally

    let (rx', ry', rotationsMouse') = rotationsForDrag dragAmounts
        modelZDelta' | isJust wheelOrPinchAmount =
                       (* mouseWheelFactor) . frint . fromJust $ wheelOrPinchAmount
                     | otherwise = 0

        modelRx' = int2Float . (`mod` 360) . floor $ modelRx + rx'
        modelRy' = int2Float . (`mod` 360) . floor $ modelRy + ry'
        modelTz' = modelTz + modelZDelta'
        (colorShader, texShader) = shaders

    -- info' $ printf "rotation y: %f" modelRy'

    (cApiGetTorchIO, cApiSetTorch) <- getCApi cApiMb app curLoop

    -- @todo this is querying the hardware on each loop.
    -- since we're holding an exclusive lock anyway on the light we could
    -- just store this value.
    lightState <- getLightState app cApiGetTorchIO

    let (warmth', warmthVal') = updateWarmth app warmth lightState
        app' = updateModelForMouse doMouseZoom doMouseRotateModel rotationsMouse' modelZDelta' app
        doFog' = doFog && not (toBool lightState)

    app'' <- drawBackground'
                 lightState jitter rand app' rectangleDimension
                 texShader texMappingCur' warmthVal' doFog' args

    debug'' "warmth: %s" warmth'
    debug'' "warmthVal: %s" warmthVal'
    debug'' "wheel or pinch: %s" wheelOrPinchAmount
    debug'' "dragAmounts: %s" dragAmounts
    debug'' "modelZDelta': %s" modelZDelta'

    let activeRegionClicked' = activeRegionClicked viewportWidth viewportHeight clickPress

    let toggleLight' | doMouseRotateModel = isJust clickRelease
                     | otherwise = activeRegionClicked'
        mappings' = (texMappingExtin, texMappingLight, texMappingCur')
        (texMapping', lightState') = updateLightState app toggleLight' lightState mappings'
    updateTorch app lightState' cApiSetTorch toggleLight'

    rand' <- tailFail "no more rands" rands

    let newLoop = curLoop { mainLoopT          = t + 1
                          , mainLoopApp'       = app'
                          , mainLoopRands      = rand'
                          , mainLoopTexMaps    = (texMappingLight, texMappingExtin, texMapping')
                          , mainLoopLightState = lightState'
                          , mainLoopWarmth     = warmth'
                          , mainLoopModelRx    = modelRx'
                          , mainLoopModelRy    = modelRy'
                          , mainLoopModelTz    = modelTz' }

        continue | qPressed  = Nothing
                 | otherwise = Just newLoop

    pure continue

drawBackground' lightState jitter rand app textureDim shader map' warmth doFog args = do
    let model' | state'    = identityMatrix
               | otherwise = translateX x'
        state' = toBool lightState
        x' = jitter * realToFrac rand
        app' = app & appMultiplyModel model'
    drawBackground app' textureDim shader map' warmth doFog args
    pure app'

drawBackground app textureDim shader texMapping warmth doFog args = do
    let tx = -0.5 * textureDim; ty = tx; tz = 2 * tx
    let scale' = 12.0
    let model' = multMatrices [ translateX tx
                              , translateY ty
                              , translateZ tz
                              , scaleX scale'
                              , scaleY scale' ]
    let app' = app & appMultiplyModel model'
    drawBackgroundTexture app' shader texMapping warmth doFog textureDim

-- | arbitrary choice of whether x is first or y, but these are small
--   amounts so it shouldn't matter too much.

rotationsForDrag :: Maybe (Int, Int) -> (Float, Float, DMat)
rotationsForDrag Nothing       = (0, 0, identityMatrix)
rotationsForDrag (Just (x, _)) = let ry' = frint x * dragXFactor
                                 in  (0, ry', multMatrices [ rotateY ry' ])

decodeImage' :: ByteString -> Either String JP.DynamicImage
decodeImage' imageBase64 = join decode' where
    decode'       = decodePng' <$> decodeBase64' imageBase64
    onError'      = first . (<>)
    decodeBase64' = onError' "Couldn't decode base64: " . B64.decode
    decodePng'    = onError' "Couldn't decode PNG: "    . JP.decodePng

decodeImage :: Log -> ByteString -> IO JP.DynamicImage
decodeImage log imageBase64 = foldM' err' ok' img where
    img    = decodeImage' imageBase64
    ok'    = pure
    err'   = die log . ("Bad image: " <>)
    foldM' = fmap toRight .:. bitraverse

initShaders log = (,) <$> initShaderColor log <*> initShaderTexture log

-- | this is used as a backup in case the dimensions are not passed in to
-- `launch`.
configYamlInline :: ByteString
configYamlInline =
    "viewportWidth: 480\n" <>
    "viewportHeight: 793\n"

drawBackgroundTexture :: App' -> Shader' -> GraphicsTextureMapping -> Float -> Bool -> Float -> IO ()
drawBackgroundTexture app shader mapping warmth doFog textureDim = do
    let log           = appLog app
        appmatrix     = appMatrix app
        prog          = shaderProgram shader
        shaderVars    = shaderShaderVars shader
        shaderMatrix' = shaderMatrix shader
        utt           = shaderVars & uniformUniformLocation . shaderVarsTUt
        udfog         = shaderVars & uniformUniformLocation . shaderVarsTUdofog
        uwarm         = shaderVars & uniformUniformLocation . shaderVarsTUwarmth
        ublur         = shaderVars & uniformUniformLocation . shaderVarsTUblur
        shaderT       = toShaderDT Nothing shaderMatrix' shaderVars
        model'        = identityMatrix
        app'          = app & appMultiplyModel model'

        -- 3rd component is always 0, 4th is always 1
        tc00 = ver4 0.1 0 0 1
        tc01 = ver4 0.1 1 0 1
        tc10 = ver4 0.9 0 0 1
        tc11 = ver4 0.9 1 0 1
        texName' = graphicsTextureMappingTextureObject mapping

    -- use the program here and set shaderT's program to Nothing.
    useShader log prog

    let doFog' | doFog = 1
               | otherwise = 0
    uniform log "do fog"    udfog $ float doFog'
    uniform log "do warmth" uwarm $ float warmth
    uniform log "blur"      ublur $ 1 - (1 - blurOnWarm) * float warmth
    rectangleTex app' shaderT textureDim textureDim texName' (tc00, tc01, tc10, tc11)

sdlGlesConfig :: SDG.Config
sdlGlesConfig = defaultConfig { output565 = True }

-- @todo export Matrix from sdl-gles-cairo
-- updateModelForMouse :: Bool -> Bool -> Matrix Float -> Float -> App' -> App'
updateModelForMouse doZoom doRotate rot zDelta = appMultiplyRightModel model' where
    model' = multMatrices mult' -- [rot, translateZ zDelta]
    mult' = concat [ if doRotate then [rot] else []
                   , if doZoom then [translateZ zDelta] else [] ]

getLightState :: App a -> CApiGetTorch -> IO LightState
getLightState app cApiGet = peek' where
    get'  = getLightState' app cApiGet
    peek' = flip withForeignPtr get' =<< mallocForeignPtr

getLightState' :: App a -> CApiGetTorch -> Ptr CInt -> IO LightState
getLightState' app cApiGet ptr = do
    let info' = info . appLog $ app
    status <- cApiCallOk <$> cApiGet ptr
    when (not status) $ info' "unable to get torch"
    fromBool . (== 1) <$> peek ptr

getCApi :: Maybe CApi -> App a -> MainLoop -> IO CApi
getCApi cApiMb | hasCApi = getCApi' cApiMb
               | otherwise = getCApiMock' cApiMb where
    hasCApi = isJust cApiMb

getCApi' :: Maybe CApi -> App a -> MainLoop -> IO CApi
getCApi' cApiMb app curLoop = do
    let info' = info . appLog $ app
    let cApiGetTorchIO = fst . fromJust $ cApiMb
        cApiSetTorch   = snd . fromJust $ cApiMb
    pure (cApiGetTorchIO, cApiSetTorch)

getCApiMock' :: Maybe CApi -> App a -> MainLoop -> IO CApi
getCApiMock' cApiMb app curLoop = do
    let info' = info . appLog $ app
    let cApiGetTorchIO = \ptr -> poke ptr getFromConfig' >> pure CApiCallOk
        cApiSetTorch   = \state -> do
            info' $ "[mock] set torch: " ++ if (toBool state) then "[on]" else "[off]"
            pure CApiCallOk
        state' = toBool $ mainLoopLightState curLoop
        getFromConfig' | state'    = 1
                       | otherwise = 0
    pure (cApiGetTorchIO, cApiSetTorch)

updateLightState :: App a -> Bool -> LightState -> GraphicsTextureMappings -> (GraphicsTextureMapping, LightState)
updateLightState app doUpdate lightState (tmExtin, tmLight, tmCur)
    | doUpdate && state'     = (tmExtin, LightOff)
    | doUpdate && not state' = (tmLight, LightOn)
    | otherwise              = (tmCur, lightState)
    where state' = toBool lightState

updateTorch :: App' -> LightState -> CApiCall -> Bool -> IO CApiCallStatus
updateTorch app state = report' <=<: updateTorch' state where
    ok' True = "ok" :: String; ok' False = "not ok" :: String
    info' = app & info . appLog
    print' ok = info' $ printf "set light to %s, %s" (show state) (ok' . cApiCallOk $ ok)
    report' (Just status) = print' status >> pure status
    report' Nothing = pure CApiCallNoop

updateTorch' :: LightState -> CApiCall -> Bool -> IO (Maybe CApiCallStatus)
updateTorch' state cApiSet = f where
    f False = pure Nothing
    f True  = Just <$> cApiSet state

updateWarmth :: App' -> Ramp -> LightState -> (Ramp, Float)
updateWarmth app warmth state = (warmth', val') where
    warmthRampDir' | state' = RampUp | otherwise = RampDown
    warmth' = updateRamp app warmthRampDir' warmth
    val'    = frint cur' / frint range' * warmthCoefficient
    cur'    = rampCur warmth'
    range'  = rampRange warmth
    state'  = toBool state

-- @todo export EventPayload from sdl-gles-cairo
-- activeRegionClicked :: Maybe (Int, Int, SDL.Event.EventPayload) -> Bool
activeRegionClicked viewportWidth' viewportHeight' = f where
    f Nothing = False
    f (Just (x, y, _)) = xOk && yOk where
        x' = frint x / frint viewportWidth'
        y' = frint y / frint viewportHeight'
        xOk = within x' (0.765, 0.871) 0.2
        yOk = within y' (0.729, 0.874) 0.2
