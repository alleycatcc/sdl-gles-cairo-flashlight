module Graphics.SGC.Shader ( initShaderTexture, initShaderColor ) where

import           Data.Monoid ( (<>) )
import qualified Data.ByteString.Char8  as BS8 ( pack )
import qualified Data.StateVar        as STV ( get )

import           Graphics.Rendering.OpenGL as GL
                 ( AttribLocation
                 , UniformLocation
                 , attribLocation
                 , uniformLocation )

import           Graphics.SDLGles.Types
                 ( Shader (Shader) )

import           Graphics.SGC.Config
                 ( isEmbedded )

import           Graphics.SGC.Types ( Shader' (Shader'C, Shader'T)
                                        , mkShaderVarsC
                                        , mkShaderVarsT
                                        , mkMatrix
                                        )

import           Graphics.SDLGles.GL.Util
                 ( wrapGL )

import           Graphics.SDLGles.GLES.Shader ( initProgram )

import qualified Graphics.SGC.ShaderSrc.VertexTexture as SVT ( shaderSrc )
import qualified Graphics.SGC.ShaderSrc.FragmentTexture as SFT ( shaderSrc )

import qualified Graphics.SGC.ShaderSrc.VertexColor as SVC ( shaderSrc )
import qualified Graphics.SGC.ShaderSrc.FragmentColor as SFC ( shaderSrc )

-- useful for developing shaders; otherwise keep to false because the FS
-- version and inline versions easily get out of sync.
allowFileSystemShaders = False

getShader = BS8.pack

initShaderTexture log =
    let vShader = getShader SVT.shaderSrc
        fShader = getShader SFT.shaderSrc
    in  Shader'T <$> initShader' log "t" vShader fShader

initShaderColor log =
    let vShader = getShader SVC.shaderSrc
        fShader = getShader SFC.shaderSrc
    in  Shader'C <$> initShader' log "c" vShader fShader

initShader' log shaderType vShaderSrc fShaderSrc = do
    prog' <- initProgram log vShaderSrc fShaderSrc
    let unif' str     = wrapGL log ("uniformLocation " <> str) . STV.get $ uniformLocation prog' str
        att' str      = wrapGL log ("attribLocation "  <> str) . STV.get $ attribLocation  prog' str
    let shaderVars'   = case shaderType of "t"  -> mkShaderVarsT
                                           "c"  -> mkShaderVarsC
    Shader <$> pure prog' <*> mkMatrix log prog' <*> shaderVars' log prog'

-- | no 'enable' necessary for uniforms.
-- • attribs need to be enabled / disabled when drawArrays is called.
-- • remember to 'use' the program in the render loop.
