module Graphics.SGC.ShaderSrc.FragmentTexture ( shaderSrc
                                                  ) where

shaderSrc =
    "#version 100\n" ++
    "#ifdef GL_ES\n" ++
    "precision mediump float;\n" ++
    "#endif\n" ++
    "\n" ++
    "uniform sampler2D texture;\n" ++
    "// --- 'boolean', but we use a float so we can multiply and avoid an if.\n" ++
    "uniform float do_vary_opacity;\n" ++
    "// --- ditto.\n" ++
    "uniform float do_fog;\n" ++
    "uniform float warmth;\n" ++
    "uniform float blur_amount;" ++
    "\n" ++
    "varying vec2 v_texcoord;\n" ++
    "varying vec4 v_position;\n" ++
    "varying vec4 v_normal;\n" ++
    "\n" ++
    "vec4 viewPos  = vec4 (-0.0, -0.0, 10.0, 1.0);\n" ++
    "\n" ++
    "float fogDensity = 2.5;\n" ++
    "float fogFactor = 0.5;\n" ++
    "float fogZFactor = 3.0;\n" ++
    "vec4 fogColor = vec4 (0.3, 0.3, 0.9, 1.0);\n" ++
    "\n" ++
    "// float finalOpacity = 0.9;\n" ++
    "\n" ++
    "struct light {\n" ++
    "    float ambientStrength;\n" ++
    "    float specularStrength;\n" ++
    "    float specularExp;\n" ++
    "    vec4 lightColor;\n" ++
    "    vec4 lightPos;\n" ++
    "};\n" ++
    "\n" ++
    "vec4 get_lighting (vec4 viewDir, vec4 norm, light l)\n" ++
    "{\n" ++
    "    vec4 lightDir = normalize (l.lightPos - v_position);\n" ++
    "    float lightProj = dot (norm, lightDir);\n" ++
    "\n" ++
    "    // xxx\n" ++
    "    lightProj = abs (lightProj);\n" ++
    "\n" ++
    "    vec4 ambient = l.ambientStrength * l.lightColor;\n" ++
    "\n" ++
    "    vec4 diffuse = max (lightProj, 0.0) * l.lightColor;\n" ++
    "    diffuse.w = l.lightColor.w;\n" ++
    "\n" ++
    "    vec4 reflectDir = reflect (-lightDir, norm);\n" ++
    "    float reflectProj = dot (viewDir, reflectDir);\n" ++
    "    float spec = pow (max (reflectProj, 0.0), l.specularExp);\n" ++
    "    vec4 specular = l.specularStrength * l.lightColor * spec;\n" ++
    "\n" ++
    "    return ambient + specular + diffuse;\n" ++
    "}\n" ++
    "\n" ++
    "vec4 blur_texture (float amount) {" ++
    "    vec2 gaussFilter0 = vec2 (-3.0, 0.015625);" ++
    "    vec2 gaussFilter1 = vec2 (-2.0, 0.09375);" ++
    "    vec2 gaussFilter2 = vec2 (-1.0, 0.234375);" ++
    "    vec2 gaussFilter3 = vec2 (-0.0, 0.3125);" ++
    "    vec2 gaussFilter4 = vec2 ( 1.0, 0.234375);" ++
    "    vec2 gaussFilter5 = vec2 ( 2.0, 0.09375);" ++
    "    vec2 gaussFilter6 = vec2 ( 3.0, 0.015625);" ++
    "    float denom = 600.0 - amount * 450.0;" ++
    "    float xblur = 1.0/denom;" ++
    "    float yblur = xblur;" ++
    "    vec2 scale = vec2 (xblur, yblur);" ++
    "    vec4 color = vec4 (0, 0, 0, 0);" ++
    "    color += texture2D( texture, vec2( v_texcoord.x+gaussFilter0.x*scale.x, v_texcoord.y+gaussFilter0.x*scale.y ) )*gaussFilter0.y;" ++
    "    color += texture2D( texture, vec2( v_texcoord.x+gaussFilter1.x*scale.x, v_texcoord.y+gaussFilter1.x*scale.y ) )*gaussFilter1.y;" ++
    "    color += texture2D( texture, vec2( v_texcoord.x+gaussFilter2.x*scale.x, v_texcoord.y+gaussFilter2.x*scale.y ) )*gaussFilter2.y;" ++
    "    color += texture2D( texture, vec2( v_texcoord.x+gaussFilter3.x*scale.x, v_texcoord.y+gaussFilter3.x*scale.y ) )*gaussFilter3.y;" ++
    "    color += texture2D( texture, vec2( v_texcoord.x+gaussFilter4.x*scale.x, v_texcoord.y+gaussFilter4.x*scale.y ) )*gaussFilter4.y;" ++
    "    color += texture2D( texture, vec2( v_texcoord.x+gaussFilter5.x*scale.x, v_texcoord.y+gaussFilter5.x*scale.y ) )*gaussFilter5.y;" ++
    "    color += texture2D( texture, vec2( v_texcoord.x+gaussFilter6.x*scale.x, v_texcoord.y+gaussFilter6.x*scale.y ) )*gaussFilter6.y;" ++
    "    return color;" ++
    "}" ++
    "vec4 normal_texture () {" ++
    "    return texture2D (texture, v_texcoord);" ++
    "}" ++
    "void main()\n" ++
    "{\n" ++
    "    light l0 = light (\n" ++
    "        0.2,\n" ++
    "        1.0,\n" ++
    "        5.0,\n" ++
    "        vec4 (0.2 + 0.45 * warmth, 0.2, 0.4 - 0.3 * warmth, 1.0),\n" ++
    "        vec4 (0.0, 0.0, 10.0, 1.0)\n" ++
    "    );\n" ++
    "\n" ++
    "    light l1 = light (\n" ++
    "        0.2,\n" ++
    "        1.0,\n" ++
    "        32.0,\n" ++
    "        vec4 (0.2, 0.2, 0.2, 0.2),\n" ++
    "        vec4 (20.0, -3.0, -4.0, 1.0)\n" ++
    "    );\n" ++
    "\n" ++
    "    vec4 init = blur_amount == 0.0 ? normal_texture () : blur_texture (blur_amount);" ++
    "\n" ++
    "    vec4 norm = normalize (v_normal);\n" ++
    "    norm.w = 0.0;\n" ++
    "\n" ++
    "    vec4 viewDir = normalize (viewPos - v_position);\n" ++
    "\n" ++
    "    vec4 lightTotal = vec4 (0.0, 0.0, 0.0, 1.0);\n" ++
    "    lightTotal += get_lighting (viewDir, norm, l0);\n" ++
    "    lightTotal += get_lighting (viewDir, norm, l1);\n" ++
    "\n" ++
    "    gl_FragColor = init * lightTotal;\n" ++
    "\n" ++
    "    float fragZ = gl_FragCoord.z / gl_FragCoord.w;\n" ++
    "\n" ++
    "    float fogCoord = pow ((fragZ / fogZFactor), 8.0) * fogFactor;\n" ++
    "\n" ++
    "    float fog = fogCoord * fogDensity * do_fog;\n" ++
    "    gl_FragColor = mix (fogColor, gl_FragColor, clamp (1.0 - fog, 0.0, 1.0));\n" ++
    "\n" ++
    "    // --- positive z = into the screen.\n" ++
    "    // --- z greater than thres: opacity = 1.0\n" ++
    "    // --- z is between 0 and thres: opacity drops off sharply\n" ++
    "    // --- z is less than 0: don't care\n" ++
    "    // --- gnuplot> plot [x=0:1] [0:1] log(x/50) + 5\n" ++
    "    // --- the x - (x - n) stuff is so we can switch on do_vary_opacity without an if.\n" ++
    "\n" ++
    "    float x = 0.8;\n" ++
    "    gl_FragColor.w = x - do_vary_opacity * (x - (log (fragZ / 50.0) + 5.0));\n" ++
    "}\n"

