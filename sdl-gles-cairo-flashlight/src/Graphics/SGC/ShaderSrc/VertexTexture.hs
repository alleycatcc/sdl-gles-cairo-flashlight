module Graphics.SGC.ShaderSrc.VertexTexture ( shaderSrc
                                                ) where

shaderSrc =
    "#version 100\n" ++
    "// ES 2.0 requires 100 or 300, which are the ES versions.\n" ++
    "\n" ++
    "uniform mat4 model;\n" ++
    "uniform mat4 view;\n" ++
    "uniform mat4 projection;\n" ++
    "\n" ++
    "uniform mat4 transpose_inverse_model;\n" ++
    "\n" ++
    "attribute vec4 a_position;\n" ++
    "attribute vec2 a_texcoord;\n" ++
    "attribute vec4 a_normal;\n" ++
    "\n" ++
    "varying vec4 v_position;\n" ++
    "varying vec2 v_texcoord;\n" ++
    "varying vec4 v_normal;\n" ++
    "\n" ++
    "void main()\n" ++
    "{\n" ++
    "    gl_Position = projection * view * model * a_position;\n" ++
    "    v_texcoord = a_texcoord;\n" ++
    "\n" ++
    "    // -- eye-space.\n" ++
    "    v_position = view * model * a_position;\n" ++
    "\n" ++
    "    vec4 normal = vec4 (vec3 (a_normal), 0.0);\n" ++
    "\n" ++
    "    // v_normal = view * transpose_inverse_model * normal;\n" ++
    "    v_normal = view * model * normal;\n" ++
    "}\n"

