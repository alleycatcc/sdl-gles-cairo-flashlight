module Graphics.SGC.Config ( doBench
                           , blurOnWarm
                           , doDebug
                           , doFog
                           , doMouseZoom
                           , doMouseRotateModel
                           , dragXFactor
                           , isEmbedded
                           , jitter
                           , mouseWheelFactor
                           , mvpConfig
                           , rectangleDimension
                           , warmthCoefficient
                           , warmthRange ) where

import           Foreign.C.Types ( CInt )
import           System.Info   as I              ( os, arch )
import           Graphics.Rendering.OpenGL as GL ( GLsizei )

import           Graphics.SDLGles.Types
                 ( ProjectionType (ProjectionFrustum, ProjectionOrtho)
                 , MVPConfig (MVPConfig) )

doDebug            = False
doBench            = False

useGLES            = isEmbedded || not useCompatibility
useCompatibility   = False
isEmbedded         = forceEmbedded || I.os == "linux_android"
forceEmbedded      = False

rectangleDimension = 0.5 :: Float

-- @todo hardcoded in sdl-gles-cairo
-- frameInterval      = 50

mvpConfigF = MVPConfig ProjectionFrustum (- 3) 1.9
-- @todo not sure if ortho is working right
mvpConfigO = MVPConfig ProjectionOrtho (- 2) 1
mvpConfig = mvpConfigF

mouseWheelFactor = 1 / 50 :: Float
dragXFactor = 2 :: Float
warmthRange = 10 :: Int
doMouseZoom = False
doMouseRotateModel = False
doFog = False
jitter = 0.00 :: Float

-- | 0 = disable blurring completely
--   0.2 = some fuzziness on warm
--   > 0.5: very fuzzy on warm
blurOnWarm = 0.20 :: Float

-- | < 0.5: cold
--   > 2.0: very warm
--   setting it too high makes it look unnatural because the gap between
--   on and off is too big.
warmthCoefficient = 1.0 :: Float
